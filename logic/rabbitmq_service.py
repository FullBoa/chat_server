import pika
from flask import current_app


class RabbitmqService(object):
    """Сервис для работы с RabbitMQ"""

    def __init__(self):
        self.exchange_name = current_app.config['RABBITMQ_EXCHANGE_NAME']
        self.durable = current_app.config['RABBITMQ_USE_DURABLE']

        host = current_app.config['RABBITMQ_HOST']
        port = current_app.config['RABBITMQ_PORT']
        credentials = None
        if 'RABBITMQ_USERNAME' in current_app.config and 'RABBITMQ_PASSWORD' in current_app.config:
            credentials = pika.credentials.PlainCredentials(
                username=current_app.config['RABBITMQ_USERNAME'],
                password=current_app.config['RABBITMQ_PASSWORD']
            )
        self.connection_parameters = pika.ConnectionParameters(
            host=host,
            port=port,
            credentials=credentials
        )

    def declare_queue(self, queue_name):
        """Объявление новой очереди"""

        with pika.BlockingConnection(self.connection_parameters) as connection:
            channel = connection.channel()
            channel.exchange_declare(
                exchange=self.exchange_name,
                type='fanout',
                durable=self.durable
            )
            channel.queue_declare(queue_name)
            channel.queue_bind(
                exchange=self.exchange_name,
                queue=queue_name,
                durable=self.durable
            )

    def put_messsage(self, message):
        """Добавление нового сообщения в exchange"""

        with pika.BlockingConnection(self.connection_parameters) as connection:
            channel = connection.channel()

            channel.basic_publish(
                exchange=self.exchange_name,
                routing_key='',
                body=message
            )

    def get_messages(self, nick):
        """Получаем все новые сообщения для пользователя"""

        with pika.BlockingConnection(self.connection_parameters) as connection:
            channel = connection.channel()

            messages = []

            method, properties, message = channel.basic_get(nick)
            while method is not None and method.NAME == 'Basic.GetOk':
                messages.append(message.decode('utf-8'))
                channel.basic_ack(method.delivery_tag)
                method, properties, message = channel.basic_get(nick)

            return messages
