import bcrypt
from dal.models.user import User
from logic.rabbitmq_service import RabbitmqService


class RegisterUserCommand(object):
    """Команда регистрации нового пользователя"""

    def __init__(self):
        self.rabbitmq_service = RabbitmqService()

    def execute(self, nick, password):

        existed_user = User.query.filter(User.nick == nick).first()
        if existed_user is not None:
            return None

        password_hash = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
        user = User()
        user.nick = nick
        user.password_hash = password_hash

        self.rabbitmq_service.declare_queue(nick)
        user.save()

        return user
