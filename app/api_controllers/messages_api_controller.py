from flask import Blueprint
from flask import jsonify
from flask import request
from logic.rabbitmq_service import RabbitmqService

messages_api_controller = Blueprint('messages_api_controller', __name__)


@messages_api_controller.route('/SEND/', methods=['POST', ])
def send():
    nick = request.args.get('nick', type=str)
    message = request.args.get('message', type=str)

    service = RabbitmqService()
    service.put_messsage('{nick} > {message}'.format(
        nick=nick,
        message=message
    ))
    return '', 200

# Запрос не является идеомпотентным, поэтому используем метод POST
@messages_api_controller.route('/RECV/', methods=['POST', ])
def receive():
    nick = request.args.get('nick', type=str)
    service = RabbitmqService()

    messages = service.get_messages(nick)

    return jsonify(messages)
