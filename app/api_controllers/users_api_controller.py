import bcrypt
from flask import Blueprint
from flask import request
from werkzeug.exceptions import Conflict, Unauthorized
from dal.models.user import User
from logic.commands.register_user_command import RegisterUserCommand

users_api_controller = Blueprint('users_api_controller', __name__)


@users_api_controller.route('/REGISTER/', methods=['POST', ])
def register():

    nick = request.args.get('nick', type=str)
    password = request.args.get('pwd', type=str)

    command = RegisterUserCommand()
    user = command.execute(nick, password)
    if user is None:
        return Conflict()

    return '', 200


@users_api_controller.route('/AUTH/', methods=['POST', ])
def auth():

    nick = request.args.get('nick', type=str)
    password = request.args.get('pwd', type=str)

    user = User.query.filter(User.nick == nick).first()
    if user is None:
        return Unauthorized()

    encode_password = password.encode('utf-8')
    encode_hash = user.password_hash.encode('utf-8')

    if bcrypt.hashpw(encode_password, encode_hash) == encode_hash:
        return '', 200
    else:
        return Unauthorized()
