from flask import Flask
from dal.database import db


def create_app(config_filename):

    app = Flask(__name__)
    app.config.from_pyfile(config_filename)
    db.init_app(app)

    from app.api_controllers.users_api_controller import users_api_controller
    from app.api_controllers.messages_api_controller import messages_api_controller
    app.register_blueprint(users_api_controller)
    app.register_blueprint(messages_api_controller)

    return app
