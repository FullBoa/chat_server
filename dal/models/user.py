from dal.database import db


class User(db.Document):

    config_collection_name = 'users'

    nick = db.StringField()
    password_hash = db.StringField()
